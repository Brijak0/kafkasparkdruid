package DatabricksSpark.DatabricksSpark;

import com.metamx.tranquility.spark.BeamFactory;
import org.apache.hadoop.fs.Hdfs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
//import kafka.serializer.StringDecoder;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;
import scala.tools.nsc.doc.model.Val;
import java.io.File;

import java.util.List;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.time.Duration;
import java.util.*;

import static java.lang.System.out;


public class DatabricksSpark 
{
    public static void main( String[] args )
    {
    	 Map<String, Object> kafkaParams = new HashMap<>();
         kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.1.5:9092");
         kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                               "org.apache.kafka.common.serialization.StringDeserializer");
         kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
         kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"test-consumer-group");
         kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
         kafkaParams.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);


         ////Configure Spark to listen messages in topic test
         Collection<String> topics = Arrays.asList("HH3261");


         //SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("DatabricksSpark").set("spark.streaming.kafka.maxRatePerPartition","1");
         
         ////Read messages in batch of 30 seconds
         /*JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(30));
                   // Start reading messages from Kafka and get DStream
         final JavaInputDStream<ConsumerRecord<String, String>> stream =
                          KafkaUtils.createDirectStream(jssc, LocationStrategies.PreferConsistent(),
                                                        ConsumerStrategies.<String,String>Subscribe(topics,kafkaParams));
         
         
         JavaDStream<String> lines = stream.map(new Function<ConsumerRecord<String,String>, String>()
         {
           @Override
              public String call(ConsumerRecord<String, String> kafkaRecord) throws Exception
              {
                   return kafkaRecord.value();
              }
          });


          lines.foreachRDD(DatabricksSpark::call);*/
         call();
    }
    
    private static void call() 
    {
    
    	//Read data from Azure storage
    	SparkContext goodSparkContext = SparkContext.getOrCreate();
    	SparkConf conf = new SparkConf().set(
    			  "fs.azure.sas.uploads.hhst0006ne.blob.core.windows.net",
    			  "?sv=2017-07-29&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-03-09T21:04:23Z&st=2018-03-09T13:04:23Z&spr=https&sig=UNWZw3LdChQYSwtiab7PgDNv561Dsl8tE%2FDPjYEv658%3D");

    	//conf.s  .hadoopConfiguration.set(
    	//		  "fs.azure.sas.uploads.hhst0006ne.blob.core.windows.net",
    	//		  "?sv=2017-07-29&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-03-09T21:04:23Z&st=2018-03-09T13:04:23Z&spr=https&sig=UNWZw3LdChQYSwtiab7PgDNv561Dsl8tE%2FDPjYEv658%3D");

    			//dbutils.fs.unmount("{mountPointPath}")
    			//dbutils.fs.mount(
    			//  source = "wasbs://uploads@hhst0006ne.blob.core.windows.net/2017-04-03/07",
    			//  mountPoint = "/mnt/uploads/2017-04-03/16",
    			//  extraConfigs = Map("fs.azure.account.key.hhst0006ne.blob.core.windows.net" -> "aSq/tQKFV39t3v5lm7ardGAx/wYqbNHiGJ616wl1HPkvXHrMHuAQuNMcjwX3sfFwNshl7FFb2s9G2ez3gsOFjA=="))
    	
    	SparkSession spark = SparkSession
                .builder()
                
                .getOrCreate();

    	Dataset<Row> df = spark.read().text("wasbs://uploads@hhst0006ne.blob.core.windows.net/2017-04-03/16/");

    	
    	//Decompress data
    	
    	
    	//Write data to Azure data lake storage
    }
    
}
