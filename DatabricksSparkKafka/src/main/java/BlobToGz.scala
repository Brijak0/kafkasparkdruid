
import java.io._
import scala.io._
import java.util.zip._

	// Spark
import org.slf4j.Logger
import org.apache.spark.{ SparkConf, SparkContext }
import org.apache.spark.sql.SparkSession;
	// Hadoop
import org.apache.hadoop.io.compress.GzipCodec
object BlobToGz {
   
	  def save(args: Array[String]): Unit = {
	    
      val spark = SparkSession
                           .builder()
                           .getOrCreate();
	    
	    val files = spark.sparkContext.binaryFiles(args(0))

	    val lines =
	      files.flatMap {
	        case (path, stream) =>
	          try {
	            val is =
	              if (path.toLowerCase.endsWith(".blob"))
	                new GZIPInputStream(stream.open)
	              else
	                stream.open

	            try {
	              Source.fromInputStream(is).getLines.toList
	            } finally {
	              try { is.close } catch { case _: Throwable => }
	            }
	          } catch {
	            case e: Throwable =>
	              //log.warn(s"error reading from ${path}: ${e.getMessage}", e)
	              List.empty[String]
	          }
	      }

	    lines.saveAsTextFile(args(1), classOf[GzipCodec])
	  }
}