import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.zip.*;

public class DecompressXml {

	
	
	public static String readAllBytesGetString(String filePath) 
	{

	    String content = "";
	    try
	    {    	
	        content = new String (Files.readAllBytes(Paths.get(filePath)));
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }

	    return content;

	}
	
	public static String decode64based(String value)
	{
		byte[] decodedBytes = Base64.getDecoder().decode(value.getBytes());
		return new String(decodedBytes);
	}
	
	public static byte[] readAllBytes(String filePath) 
	{

	    byte[] content = null;
	    try
	    {
    	
	        content = Files.readAllBytes(Paths.get(filePath));
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }

	    return content;

	}
	
}
