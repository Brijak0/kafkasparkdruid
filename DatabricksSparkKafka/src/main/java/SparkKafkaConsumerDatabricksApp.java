
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.input.PortableDataStream;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Encoders;


import scala.Tuple2;


public class SparkKafkaConsumerDatabricksApp {
 public static void main(String[] argv) throws Exception{


           // Configure Spark to connect to Kafka running on local machine
         /* Map<String, Object> kafkaParams = new HashMap<>();
          kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.1.5:9092");
          kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                                "org.apache.kafka.common.serialization.StringDeserializer");
          kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
          kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"test-consumer-group");
          kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
          kafkaParams.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);


          ////Configure Spark to listen messages in topic test
          Collection<String> topics = Arrays.asList("HH3187");


          SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("SparkKafkaConsumerApp").set("spark.streaming.kafka.maxRatePerPartition","1");


          ////Read messages in batch of 30 seconds
          JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(30));
                    // Start reading messages from Kafka and get DStream
          final JavaInputDStream<ConsumerRecord<String, String>> stream =
                           KafkaUtils.createDirectStream(jssc, LocationStrategies.PreferConsistent(),
                                                         ConsumerStrategies.<String,String>Subscribe(topics,kafkaParams));


           // Read value of each message from Kafka and return it
           JavaDStream<String> lines = stream.map(new Function<ConsumerRecord<String,String>, String>()
           {
             @Override
                public String call(ConsumerRecord<String, String> kafkaRecord) throws Exception
                {
                     return kafkaRecord.value();
                }
            });


            lines.foreachRDD(SparkKafkaConsumerDatabricksApp::call);*/
            
            /*try {
            	lines.foreachRDD(SparkKafkaConsumerApp::DruidBeam);
            }
            catch(Exception ex)
            {
            	System.out.println("Exception: " + ex);
            }*/

           decompress();

          // jssc.start();
          // jssc.awaitTermination();
    }

 
 	private static void decompress()
 	{
 		
 		
 	     //spark.sparkContext.hadoopConfiguration.set(
 		 //  "fs.azure.account.key.hhst0006ne.blob.core.windows.net",
 		//  "aSq/tQKFV39t3v5lm7ardGAx/wYqbNHiGJ616wl1HPkvXHrMHuAQuNMcjwX3sfFwNshl7FFb2s9G2ez3gsOFjA=="
 		//)
 		 /*SparkSession spark = SparkSession
         .builder()
         .appName("interfacing spark sql to hive metastore without configuration file")
         .config("hive.metastore.uris", "thrift://192.168.1.5:9083") // replace with your hivemetastore service's thrift url
         //.enableHiveSupport() // don't forget to enable hive support
         .getOrCreate();*/
 		 
 		SparkSession spark = SparkSession
                           .builder()
                           .getOrCreate();

 		spark.conf().set("fs.azure.sas.uploads.hhst0006ne.blob.core.windows.net",
 				  "?sv=2017-07-29&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-03-16T14:40:08Z&st=2018-03-14T06:40:08Z&spr=https&sig=1an4oEmLBYxR25x6rGIVxYot9vAfmKfKrqdyW8FDoYQ%3D");
 		spark.sparkContext().hadoopConfiguration().set(
 				  "fs.azure.sas.uploads.hhst0006ne.blob.core.windows.net",
 				  "?sv=2017-07-29&ss=bfqt&srt=sco&sp=rwdlacup&se=2018-03-16T14:40:08Z&st=2018-03-14T06:40:08Z&spr=https&sig=1an4oEmLBYxR25x6rGIVxYot9vAfmKfKrqdyW8FDoYQ%3D");
			
 		
 				
 		
 		//Dataset<String> df = spark.read().textFile("wasbs://uploads@hhst0006ne.blob.core.windows.net/2017-04-03/16/20170403_160332283_F5375314-8718-E711-80C3-00155D675BAF.blob");
 		System.out.println("Scala obj call ");			
 		//RDD<Tuple2<String, PortableDataStream>> files = spark.sparkContext().binaryFiles("wasbs://uploads@hhst0006ne.blob.core.windows.net/2017-04-03/16/20170403_160332283_F5375314-8718-E711-80C3-00155D675BAF.blob",1); 
 		String[] arguments = {"wasbs://uploads@hhst0006ne.blob.core.windows.net/2017-04-03/16/20170403_160332283_F5375314-8718-E711-80C3-00155D675BAF.blob","20170403/16/Data8"};	 
 		BlobToGz.save(arguments); 
 		System.out.println("Read gz file in java ");	
 		Dataset<String> df  = spark.read().textFile("20170403/16/Data8");
 		List<String> listOne = df.as(Encoders.STRING()).collectAsList();
 		for (String value : listOne)
 		{
 			System.out.println(DecompressXml.decode64based(value));
 		}
 		
 			    
 			     
 			    //Dataset<String> file = spark.read().textFile("Data5");
 			    //List<String> listOne = file.as(Encoders.STRING()).collectAsList();
 			     //var listOneAsList = listOne
 			     //val builder1 = StringBuilder.newBuilder
 			    // Call addString to add all strings with no separator.
 			    //listOne.addString(builder1)
 			    //val  result1 = builder1.toString()
 			   // val listByte = listOne.map(_.getBytes())
 			//println(for {s <- List("One", "Two", "Three")} yield s) }
 			     
 	          //listOne.foreach((i: String) => println(java.util.Base64.getDecoder.decode(i.getBytes())))
 		
 
  		// define the destination
 		/*String dest = "/tmp/importSkills";
 		System.out.println("write file ");	
 		// write the data
 		String orcFile = dest + "/data.gz";
 		//.format("gz")
 		df.write().mode(SaveMode.Overwrite).save(orcFile);
 		
 		System.out.println("read file ");
 		
 		JavaRDD<String> orJ = sc.textFile(orcFile);		
 		for(String line:orJ.collect()){
            System.out.println(uncompressString(line.getBytes()));
 		}
 		
 		System.out.println("df foreach");*/
 		
 		/*String[] listOne = (String[]) df.as(Encoders.STRING()).collect();
 		//List<Row> dt = df.select("value").collectAsList();
 		System.out.println("listone foreach umcompress");
 		List<String> listStr = Arrays.asList(listOne);
 		for (String item : listStr) {
 			System.out.println(uncompressString(item.getBytes()));
 		}*/
 		
       
 		//df.foreach(e->uncompressString(e.getBytes()));
        // collect RDD for printing
        /*for(String dfLine:df.collect()){
            System.out.println(dfLine);
            String uncompress = uncompressString(dfLine.getBytes());
			System.out.println(uncompress);
			byte[] decodedBytes = Base64.getDecoder().decode(uncompress.getBytes());			
			System.out.println("decodedBytes " + new String(decodedBytes));
        }*/

        
 		
 		 /*List<String> listOne = df.as(Encoders.STRING()).collectAsList();
 		 System.out.println(listOne);
	    //using df.map
 		
 		 //List<Row> list = df.collectAsList();
 		 System.out.println("Decompressing list one");
 		
 		 listOne.forEach(str -> {
			try {
				System.out.println("Decompress");
				System.out.println(str);
				String uncompress = uncompressString(str.getBytes());
				System.out.println(uncompress);
				byte[] decodedBytes = Base64.getDecoder().decode(uncompress.getBytes());			
				System.out.println("decodedBytes " + new String(decodedBytes));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});*/
 		 
 		

 	}
 	public static String uncompressString(byte[] compressedString) throws IllegalArgumentException, IllegalStateException {
        if (compressedString == null) {
            throw new IllegalArgumentException("The compressed string specified was null.");
        }
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(compressedString);
            GZIPInputStream gzipInputStream = new GZIPInputStream(bais);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int value = 0; value != -1;) {
                value = gzipInputStream.read();
                if (value != -1) {
                    baos.write(value);
                }
            }
            gzipInputStream.close();
            baos.close();
            
            return new String(baos.toByteArray());
        }
        catch (Exception e) {
            throw new IllegalStateException("GZIP uncompression failed: " + e, e);
        }
    }
 	
 	
 	
    private static void call(JavaRDD<String> rdd) {
        int il;
        SparkConf conf1 = new SparkConf().setMaster("local[*]").setAppName("SparkKafkaConsumerApp");
       // SQLContext sqlContext = new SQLContext(SparkContext.getOrCreate(conf1));
         //hiveSqlContext = new HiveContext(conf1);

        SparkSession spark = SparkSession
                .builder()
                .getOrCreate();	

        Dataset<Row> df = spark.read().json(rdd);
                       
        df.printSchema();
        df.createOrReplaceTempView("assignments");
        df.show();

        try {

            List<Row> dt = df.select("created").collectAsList();
            Dataset dataset = df.select("created");
            dataset.show();
            Row row  = df.select("created").first();
            String str = GetFileName(dt.get(0).getString(0));

            System.out.println("Created: " + str);
            String path = "/tmp/importservice1/" + str + ".json";
            String cat = GetCatName(dt.get(0).getString(0));
            String pathJson = "/tmp/importservice1/json/" +cat+"/"+ str + ".json";
            System.out.println("Path: " + path);

            df.write().mode(SaveMode.Overwrite).json(pathJson);
            df.write().insertInto("jsonv2");
            
            
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    private static void DruidBeam(JavaRDD<String> rdd) {

    	System.out.println("In the beam section");
    	SparkSession spark = SparkSession
                .builder()
                .appName("interfacing spark sql to hive metastore without configuration file")
                .config("hive.metastore.uris", "thrift://hh3-m0.vkcvlvqqaieujcpt4lgxtrkpia.fx.internal.cloudapp.net:9083") // replace with your hivemetastore service's thrift url
                .enableHiveSupport() // don't forget to enable hive support
                .getOrCreate();

    	Dataset<Row> dataSet = spark.read().json(rdd);
    	dataSet.foreach((ForeachFunction<Row>) row -> doBeam(row));
    			
    	


    }

    private static void doBeam(Row row)
    {
    	System.out.println("Do beam");
    	String timestamp = row.getString(row.fieldIndex("created"));
		String institution_id = row.getString(row.fieldIndex("institution_id"));
		String org_id = row.getString(row.fieldIndex("org_id"));
		String questionId = "-1";
		if (row.getString(row.fieldIndex("questionId")) != null) {
			questionId = row.getString(row.fieldIndex("questionId"));
		}
		String user_id = row.getString(row.fieldIndex("user_id"));
		String total_questions = row.getString(row.fieldIndex("total_questions"));
		String score = String.valueOf(row.getDouble(row.fieldIndex("score")));

        //GetFileName(row.get(0).getString(0));
        String str =
        row.mkString("[{",",","}]");
        System.out.println("Timestamp: " + timestamp +" data: "+str);
        SimpleEventBeam.TestBeam(timestamp,institution_id, org_id, questionId, user_id, total_questions, score);
    }
    
    private static String GetFileName(String fileName)
    {
        String result = null;

         fileName = fileName.replaceAll("-","");
         fileName = fileName.replaceAll(":","");
         result = fileName;

        return result;
    }

    private static String GetCatName(String fileName)
    {
        String result = null;

        fileName = fileName.replaceAll("-","");
        fileName = fileName.replaceAll(":","");
        fileName = fileName.replaceAll(".","");
        String[] fileNameArr = fileName.split("T");

        result = fileNameArr[0];

        return result;
    }

}



