import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.zip.*;


public class JavaTestDeCompress {
	
	public static void main(String[] argv) throws Exception{
		//String str = readAllBytesJava7("c:\\temp\\20170403_164415494_9D626DC5-8C18-E711-80C3-00155D675BAF.blob");
		byte[] bytes = readAllBytes("c:\\temp\\20170403_164415494_9D626DC5-8C18-E711-80C3-00155D675BAF.blob"); 
		//String decomp = decompress(str);
		String decomp = uncompressString(bytes);
		System.out.println(decomp);
		byte[] decodedBytes = Base64.getDecoder().decode(decomp.getBytes());			
		System.out.println("decodedBytes " + new String(decodedBytes));
		ZipInputStream stream = new ZipInputStream( new ByteArrayInputStream(decodedBytes));
		
		//java.util.zip.ZipInputStream
		ZipEntry entry = null;
        while ( (entry = stream.getNextEntry()) != null ) {
         System.out.println( entry.getName());
        }

		
	}
	private static String readAllBytesJava7(String filePath) 
	{

	    String content = "";
	    try
	    {
    	
	        content = new String (Files.readAllBytes(Paths.get(filePath)));
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }

	    return content;

	}
	
	private static byte[] readAllBytes(String filePath) 
	{

	    byte[] content = null;
	    try
	    {
    	
	        content = Files.readAllBytes(Paths.get(filePath));
	    } 
	    catch (IOException e) 
	    {
	        e.printStackTrace();
	    }

	    return content;

	}
	public static String uncompressString(byte[] compressedString) throws IllegalArgumentException, IllegalStateException {
        if (compressedString == null) {
            throw new IllegalArgumentException("The compressed string specified was null.");
        }
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(compressedString);
            GZIPInputStream gzipInputStream = new GZIPInputStream(bais);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int value = 0; value != -1;) {
                value = gzipInputStream.read();
                if (value != -1) {
                    baos.write(value);
                }
            }
            gzipInputStream.close();
            baos.close();
            return new String(baos.toByteArray());
        }
        catch (Exception e) {
            throw new IllegalStateException("GZIP uncompression failed: " + e, e);
        }
    }

	
	public static String decompress(String bytes) throws Exception {
 	    
	       byte[] b = bytes.getBytes(Charset.forName("UTF-8"));
		
	 	   GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(b));
	 	   BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
	       String outStr = "";
	       String line;
	       while ((line=bf.readLine())!=null) {
	         outStr += line;
	       }
	       System.out.println("Output String lenght : " + outStr.length());
	       System.out.println("String : " + outStr);
	       return outStr;

	 	}

}
