import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.metamx.common.Granularity;
import com.metamx.common.logger.Logger;
import com.metamx.tranquility.beam.ClusteredBeamTuning;
import com.metamx.tranquility.druid.DruidBeams;
import com.metamx.tranquility.druid.DruidDimensions;
import com.metamx.tranquility.druid.DruidLocation;
import com.metamx.tranquility.druid.DruidRollup;
import com.metamx.tranquility.tranquilizer.Tranquilizer;
import com.metamx.tranquility.typeclass.Timestamper;
import com.twitter.util.Await;
import com.twitter.util.Future;
import io.druid.data.input.impl.TimestampSpec;
import io.druid.granularity.QueryGranularity;
import io.druid.query.aggregation.AggregatorFactory;
import io.druid.query.aggregation.CountAggregatorFactory;
import io.druid.query.aggregation.LongSumAggregatorFactory;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.joda.time.DateTime;
import org.joda.time.Period;
import scala.runtime.BoxedUnit;

import java.util.List;
import java.util.Map;

import static java.lang.String.*;


public class SimpleEventBeam
{
    private static final Logger log = new Logger(SimpleEventBeam.class);
    public static void TestBeam(String time, String institution_id,String org_id,  String questionId,
                                String user_id, String total_questions, String score )
    {
        System.out.println("Inside test beam: ");
         final String indexService = "druid/overlord"; // Your overlord's druid.service
         final String discoveryPath = "/druid/discovery"; // Your overlord's druid.discovery.curator.path
         final String dataSource = "ImportServiceData";
         final List<String> dimensions = ImmutableList.of("institution_id", "org_id", "questionId",
                    "user_id", "total_questions");
         final List<AggregatorFactory> aggregators = ImmutableList.of(
                   new CountAggregatorFactory("cnt"),
                   new LongSumAggregatorFactory("score", "score")
         );

        System.out.println("Timestamping: ");
         // Tranquility needs to be able to extract timestamps from your object type (in this case, Map<String, Object>).
         final Timestamper<Map<String, Object>> timestamper = new Timestamper<Map<String, Object>>()
         {
             @Override
              public DateTime timestamp(Map<String, Object> theMap)
               {
                     return new DateTime(theMap.get("timestamp"));
                   }
             };

        System.out.println("CuratorFramwork: ");
         // Tranquility uses ZooKeeper (through Curator) for coordination.
         final CuratorFramework curator = CuratorFrameworkFactory
             .builder()
             .connectString("localhost:2181")
             .retryPolicy(new ExponentialBackoffRetry(1000, 20, 30000))
             .build();
         curator.start();

        System.out.println("Serializer: ");
         // The JSON serialization of your object must have a timestamp field in a format that Druid understands. By default,
         // Druid expects the field to be called "timestamp" and to be an ISO8601 timestamp.
         final TimestampSpec timestampSpec = new TimestampSpec("timestamp", "auto", null);

        System.out.println("Tranquility: ");
         // Tranquility needs to be able to serialize your object type to JSON for transmission to Druid. By default this is
         // done with Jackson. If you want to provide an alternate serializer, you can provide your own via ```.objectWriter(...)```.
         // In this case, we won't provide one, so we're just using Jackson.
         Period per = Period.years(1);
         final Tranquilizer<Map<String, Object>> druidService = DruidBeams
             .builder(timestamper)
             .curator(curator)
             .discoveryPath(discoveryPath)
             .location(DruidLocation.create(indexService, dataSource))
            .timestampSpec(timestampSpec)
            .rollup(DruidRollup.create(DruidDimensions.specific(dimensions), aggregators, QueryGranularity.fromString("MINUTE")))
             .tuning(
                 ClusteredBeamTuning
                     .builder()
                            .segmentGranularity(Granularity.YEAR)
                            .windowPeriod(per)
                            .partitions(1)
                             .replicants(1)
                             .build()
                     )
             .buildTranquilizer();

        System.out.println("Start druid service: ");
         druidService.start();


         try {
               // Build a sample event to send; make sure we use a current date
             Map<String,Object> myMap = ImmutableMap.<String, Object>builder()
                .put("timestamp", time)
                .put("institution_id", institution_id)
                .put("org_id", org_id)
                .put("questionId", questionId)
                .put("user_id", user_id)
                .put("total_questions", total_questions)
                .put("score", score)
                .build();
//builder;
               //Map<String, Object> obj = myMap;
                     //   ImmutableMap.<String, Object>of(
                     //  "created", timestamp,
                     //  "institution_id", institution_id
                     //  "org_id", org_id
                     //  "questionId", questionId
                     //  "user_id", user_id
                     //  "total_questions", total_questions
                     //  "score", score
                     //   );

              //Dataset<Row> df = spark.read().json(rdd);

             System.out.println("Send event to druid: ");
             // Send event to Druid:
               final Future<BoxedUnit> future = druidService.send(myMap);

             System.out.println("Await result: ");
             // Wait for confirmation:
               Await.result(future);
             }
         catch (Exception e) {
             System.out.println(format("Failed to send message: {0} ",e));
              log.warn(e, "Failed to send message");
         }
         finally {
               // Close objects:
               druidService.stop();
               curator.close();
             }
       }


}



