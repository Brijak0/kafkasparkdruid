import org.apache.kafka.common.*;
import org.apache.kafka.clients.consumer.*;
import kafka.serializer.StringDecoder;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;




import java.util.*;

public class ConsumerApp {
    public static void main(String[] args){

        // Create the Properties class to instantiate the Consumer with the desired settings:
        //  Properties props = new Properties();
        //  props.put("bootstrap.servers", "192.168.1.5:6667");
        //  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        //  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        //  props.put("fetch.min.bytes", 1);
        //  props.put("group.id", "");
        //  props.put("heartbeat.interval.ms", 3000);
        //  props.put("max.partition.fetch.bytes", 1048576);
        //  props.put("session.timeout.ms", 30000);
        //  props.put("auto.offset.reset", "latest");
        //  props.put("connections.max.idle.ms", 540000);
        //  props.put("enable.auto.commit", true);
        //props.put("exclude.internal.topics", true);
        //props.put("max.poll.records", 2147483647);
        //  props.put("partition.assignment.strategy", "org.apache.kafka.clients.consumer.RangeAssignor");
        //  props.put("request.timeout.ms", 40000);
        //  props.put("auto.commit.interval.ms", 5000);
        //  props.put("fetch.max.wait.ms", 500);
        // props.put("metadata.max.age.ms", 300000);
        //  props.put("reconnect.backoff.ms", 50);
        //  props.put("retry.backoff.ms", 100);
        //  props.put("client.id", "");


        // Create a KafkaConsumer instance and configure it with properties.
        // KafkaConsumer<String, String> myConsumer = new KafkaConsumer<String, String>(props);

        // Create a topic subscription list:
        // ArrayList<TopicPartition> partitions = new ArrayList<TopicPartition>();
        //  partitions.add(new TopicPartition("HH3187", 0)); // Adds a TopicPartition instance representing a topic and a partition.
        //partitions.add(new TopicPartition("HH3178", 2)); // Adds an additional TopicPartition instance representing a different partition within the topic. Change as desired.
        // Assign partitions to the Consumer:
        //  myConsumer.assign(partitions);

        // Retrieves the topic subscription list from the SubscriptionState internal object:
        // Set<TopicPartition> assignedPartitions = myConsumer.assignment();

        // Print the partition assignments:
        // printSet(assignedPartitions);
        //myConsumer.subscribe(Arrays.asList("my-topic"));
        // Start polling for messages:
        // try {
        //    while (true){
        //        ConsumerRecords records = myConsumer.poll(1000);
        //        printRecords(records);
        //    }


        //  }
        //   finally {
        //      myConsumer.close();
        //  }

        SparkConf conf = new SparkConf()
                .setAppName("kafka-sandbox")
                .setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaStreamingContext ssc = new JavaStreamingContext(sc, new Duration(2000));

        Set<String> topics = Collections.singleton("HH3187");
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put("metadata.broker.list", "192.168.1.5:9092");
        //implicit val stringpEncoder = org.apache.spark.sql.Encoders.kryo[String]
        JavaPairInputDStream<String, String> directKafkaStream = KafkaUtils.createDirectStream(ssc,
                String.class, String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topics);

        directKafkaStream.foreachRDD(rdd -> {
            System.out.println("--- New RDD with " + rdd.partitions().size()
                    + " partitions and " + rdd.count() + " records" );
            rdd.foreach(record -> System.out.println(record._2));
        });

        ssc.start();
        ssc.awaitTermination();

    }

    private static void printSet(Set<TopicPartition> collection){
        if (collection.isEmpty()) {
            System.out.println("I do not have any partitions assigned yet...");
        }
        else {
            System.out.println("I am assigned to following partitions:");
            for (TopicPartition partition: collection){
                System.out.println(String.format("Partition: %s in Topic: %s", Integer.toString(partition.partition()), partition.topic()));
            }
        }
    }

    private static void printRecords(ConsumerRecords<String, String> records)
    {
        for (ConsumerRecord<String, String> record : records) {
            System.out.println(String.format("Topic: %s, Partition: %d, Offset: %d, Key: %s, Value: %s", record.topic(), record.partition(), record.offset(), record.key(), record.value()));
        }
    }
}
