import org.apache.kafka.clients.producer.*;

import java.text.*;
import java.util.*;

public class ProducerApp {

    public static void main(String[] args){

        // Create the Properties class to instantiate the Consumer with the desired settings: localhost:9092
        Properties props = new Properties();
        props.put("bootstrap.servers", "sandbox-hdp.hortonworks.com:6667");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        //props.put("acks", "");
        //props.put("buffer.memory", 33554432);
        //props.put("compression.type", "none");
        //props.put("retries", 0);
        //props.put("batch.size", 16384);
        //props.put("client.id", "");
        //props.put("linger.ms", 0);
        //props.put("max.block.ms", 60000);
        //props.put("max.request.size", 1048576);
        //props.put("partitioner.class", "org.apache.kafka.clients.producer.internals.DefaultPartitioner");
        //props.put("request.timeout.ms", 30000);
        //props.put("timeout.ms", 30000);
        //props.put("max.in.flight.requests.per.connection", 5);
        //props.put("retry.backoff.ms", 5);
        String topic = "my-topic";
        KafkaProducer<String, String> myProducer = new KafkaProducer<String, String>(props);
        DateFormat dtFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");

        //System.out.println(String.format("Kafka producer created %s Dt: %s", myProducer.partitionsFor(topic), dtFormat.format(new Date())));



        int numberOfRecords = 1000; // number of records to send
        long sleepTimer = 250; // how long you want to wait before the next record to be sent
        String num = "";
        try {
            for (int i = 0; i < numberOfRecords; i++ ) {
                num = Integer.toString(i);
                System.out.println(String.format("Sending message %s", num));
                myProducer.send(new ProducerRecord<String, String>(topic, String.format("Message: %s  sent at %s", num, dtFormat.format(new Date()))));
                Thread.sleep(sleepTimer);
            }
            // Thread.sleep(new Random(5000).nextLong()); // use if you want to randomize the time between record sends
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            myProducer.close();
        }

    }
}
