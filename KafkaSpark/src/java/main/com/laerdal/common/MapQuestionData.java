package src.java.main.com.laerdal.common;

import src.java.main.com.laerdal.domain.QuestionData;
import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component

public abstract class MapQuestionData implements Function<String, QuestionData> {
    private static Logger logger = LoggerFactory.getLogger(MapQuestionData.class);

    @Override
    public QuestionData quest(String line) throws Exception {
        QuestionData quest = new QuestionData();

        // split on, but not inside, quoted field
        String[] columns = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        if (columns.length < 25) {
            String[] values = {"", "", "", "", "", "", "","", "", "", "", "", "", "","", "", "", "", "", "", "","", "", "", ""}; // 7 values
            System.arraycopy(columns, 0, values, 0, columns.length);

            quest.setId(values[0]);
            quest.setName(values[1]);
            quest.setHeadline(values[2]);
            quest.setText(values[3]);
            quest.setProduct_id(values[4]);
            quest.setShow_evaluations(values[5]);
            quest.setTotal_questions(values[6]);
            quest.setRandom_sections(values[7]);
            quest.setRandom_questions(values[8]);
            quest.setCss(values[9]);
            quest.setDbName(values[10]);
            quest.setToken(values[11]);
            quest.setQuestionId(values[12]);
            quest.setSelectedOptionId(values[13]);
            quest.setIsCorrectOption(values[14]);
            quest.setTimestamp(values[15]);
            quest.setUser_id(values[16]);
            quest.setCompleted(values[17]);
            quest.setStep_start_date(values[18]);
            quest.setScore(values[19]);
            quest.setStep_duration(values[20]);
            quest.setManifest_id(values[21]);
            quest.setOrg_id(values[22]);
            quest.setInstitution_id(values[23]);


        } else {
            logger.warn("bad row " + line);
        }
        return quest;

    }


}