package src.java.main.com.laerdal.domain;

import java.io.Serializable;

public class QuestionData implements Serializable {
    static final long serialVersionUID = 1000L;

    private String id;
    private String name;
    private String headline;
    private String text;
    private String product_id;
    private String show_evaluations;
    private String total_questions;
    private String random_sections;
    private String random_questions;
    private String css;
    private String dbName;
    private String token;
    private String questionId;
    private String selectedOptionId;
    private String isCorrectOption;
    private String timestamp;
    private String user_id;
    private String completed;
    private String step_start_date;
    private String score;
    private String step_duration;
    private String manifest_id;
    private String org_id;
    private String institution_id;


    public String getId() {return id;}
    public void setId(String id){this.id = id;}

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public String getHeadline(){return headline;}
    public void setHeadline(String headline){this.headline = headline;}

    public String getText() {return text;}
    public void setText(String text) {this.text = text;}

    public String getProduct_id(){return product_id;}
    public void setProduct_id(String product_id) {this.product_id = product_id;}

    public String getShow_evaluations(){return show_evaluations;}
    public void setShow_evaluations(String show_evaluations) {this.show_evaluations = show_evaluations;}

    public String getTotal_questions(){return total_questions;}
    public void setTotal_questions(String total_questions) {this.total_questions = total_questions;}

    public String getRandom_sections(){return random_sections;}
    public void setRandom_sections(String random_sections) {this.random_sections = random_sections;}

    public String getRandom_questions(){return random_questions;}
    public void setRandom_questions(String random_questions) {this.random_questions = random_questions;}

    public String getCss(){return css;}
    public void setCss(String css) {this.css = css;}

    public String getDbName(){return dbName;};
    public void setDbName(String dbName) {this.dbName = dbName;}

    public String getToken(){return token;};
    public void setToken(String token) {this.token = token;}

    public String getQuestionId(){return questionId;};
    public void setQuestionId(String questionId) {this.questionId = questionId;}

    public String getSelectedOptionId(){return selectedOptionId;}
    public void setSelectedOptionId(String selectedOptionId) {this.selectedOptionId = selectedOptionId;}

    public String getIsCorrectOption(){return isCorrectOption;}
    public void setIsCorrectOption(String isCorrectOption) {this.isCorrectOption = isCorrectOption;}

    public String getTimestamp(){return timestamp;}
    public void setTimestamp(String timestamp) {this.timestamp = timestamp;}

    public String getUser_id(){return user_id;}
    public void setUser_id(String user_id) {this.user_id = user_id;}

    public String getCompleted(){return completed;}
    public void setCompleted(String completed) {this.completed = completed;}

    public String getStep_start_date(){return step_start_date;}
    public void setStep_start_date(String step_start_date) {this.step_start_date = step_start_date;}

    public String getScore(){return score;}
    public void setScore(String score) {this.score = score;}

    public String getStep_duration(){return step_duration;}
    public void setStep_duration(String step_duration) {this.step_duration = step_duration;}

    public String getManifest_id(){return manifest_id;}
    public void setManifest_id(String manifest_id) {this.manifest_id = manifest_id;}

    public String getOrg_id(){return org_id;}
    public void setOrg_id(String org_id) {this.org_id = org_id;}

    public String getInstitution_id(){return institution_id;}
    public void setInstitution_id(String institution_id) {this.institution_id = institution_id;}


}