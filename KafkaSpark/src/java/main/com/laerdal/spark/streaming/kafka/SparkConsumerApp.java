package src.java.main.com.laerdal.spark.streaming.kafka;

import org.apache.kafka.common.*;
import org.apache.kafka.clients.consumer.*;
import org.springframework.beans.factory.annotation.Autowired;
import src.java.main.com.laerdal.common.MapQuestionData;
import src.java.main.com.laerdal.domain.QuestionData;
import kafka.serializer.StringDecoder;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka.KafkaUtils;
import scala.Tuple2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import java.util.*;


 import org.apache.spark.api.java.function.Function;




public class SparkConsumerApp {
    @Autowired
    MapQuestionData mapQuestionData;
    public static void main(String[] args) throws InterruptedException {



        SparkConf conf = new SparkConf()
                .setAppName("kafka-sandbox")
                .setMaster("local[*]");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaStreamingContext ssc = new JavaStreamingContext(sc, new Duration(2000));

        Set<String> topics = Collections.singleton("HH3187");
        Map<String, String> kafkaParams = new HashMap<>();
        kafkaParams.put("metadata.broker.list", "192.168.1.5:9092");
        //implicit val stringpEncoder = org.apache.spark.sql.Encoders.kryo[String]
        JavaPairInputDStream<String, String> directKafkaStream = KafkaUtils.createDirectStream(ssc,
                String.class, String.class, StringDecoder.class, StringDecoder.class, kafkaParams, topics);



        /*directKafkaStream.foreachRDD(rdd -> {
            System.out.println("--- New RDD with " + rdd.partitions().size()
                    + " partitions and " + rdd.count() + " records" );
            rdd.foreach(record -> System.out.println(record._1()));
            rdd.foreach(record -> System.out.println(record._2()));
        });*/
        JavaDStream<String> streamLines = directKafkaStream.map((Function<Tuple2<String,String>,String>)Tuple2::_2);
        JavaDStream<QuestionData> questData = streamLines.map(mapQuestionData);

        //streamLines.foreachRDD(rdd->{System.out.println("Read line"+ rdd.toString());});
        //Function<Tuple2<String, String>, String>) Tuple2::_2);
        //directKafkaStream.saveAsHadoopFiles();print();

        ssc.start();
        ssc.awaitTermination();
        ssc.stop();
    }
}
