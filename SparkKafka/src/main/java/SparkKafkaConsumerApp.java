
import com.metamx.tranquility.spark.BeamFactory;
import org.apache.hadoop.fs.Hdfs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.*;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
//import kafka.serializer.StringDecoder;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;
import scala.tools.nsc.doc.model.Val;
import java.io.File;

import java.util.List;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.time.Duration;
import java.util.*;

import static java.lang.System.out;


public class SparkKafkaConsumerApp {
 public static void main(String[] argv) throws Exception{


           // Configure Spark to connect to Kafka running on local machine
          Map<String, Object> kafkaParams = new HashMap<>();
          kafkaParams.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.1.5:9092");
          kafkaParams.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                                "org.apache.kafka.common.serialization.StringDeserializer");
          kafkaParams.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
          kafkaParams.put(ConsumerConfig.GROUP_ID_CONFIG,"test-consumer-group");
          kafkaParams.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
          kafkaParams.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,true);


          ////Configure Spark to listen messages in topic test
          Collection<String> topics = Arrays.asList("HH3187");


          SparkConf conf = new SparkConf().setMaster("local[*]").setAppName("SparkKafkaConsumerApp").set("spark.streaming.kafka.maxRatePerPartition","1");


          ////Read messages in batch of 30 seconds
          JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(30));
                    // Start reading messages from Kafka and get DStream
          final JavaInputDStream<ConsumerRecord<String, String>> stream =
                           KafkaUtils.createDirectStream(jssc, LocationStrategies.PreferConsistent(),
                                                         ConsumerStrategies.<String,String>Subscribe(topics,kafkaParams));


           // Read value of each message from Kafka and return it
           JavaDStream<String> lines = stream.map(new Function<ConsumerRecord<String,String>, String>()
           {
             @Override
                public String call(ConsumerRecord<String, String> kafkaRecord) throws Exception
                {
                     return kafkaRecord.value();
                }
            });


            lines.foreachRDD(SparkKafkaConsumerApp::call);
            try {
            	lines.foreachRDD(SparkKafkaConsumerApp::DruidBeam);
            }
            catch(Exception ex)
            {
            	System.out.println("Exception: " + ex);
            }

          

           jssc.start();
           jssc.awaitTermination();
    }

    private static void call(JavaRDD<String> rdd) {
        int il;
        SparkConf conf1 = new SparkConf().setMaster("local[*]").setAppName("SparkKafkaConsumerApp");
       // SQLContext sqlContext = new SQLContext(SparkContext.getOrCreate(conf1));
         //hiveSqlContext = new HiveContext(conf1);

        SparkSession spark = SparkSession
                .builder()
                .appName("interfacing spark sql to hive metastore without configuration file")
                .config("hive.metastore.uris", "thrift://hh3-m0.vkcvlvqqaieujcpt4lgxtrkpia.fx.internal.cloudapp.net:9083") // replace with your hivemetastore service's thrift url
                .enableHiveSupport() // don't forget to enable hive support
                .getOrCreate();



        Dataset<Row> df = spark.read().json(rdd);
                       
        df.printSchema();
        df.createOrReplaceTempView("assignments");
        df.show();

        try {

            List<Row> dt = df.select("created").collectAsList();
            Dataset dataset = df.select("created");
            dataset.show();
            Row row  = df.select("created").first();
            String str = GetFileName(dt.get(0).getString(0));

            System.out.println("Created: " + str);
            String path = "/tmp/importservice1/" + str + ".json";
            String cat = GetCatName(dt.get(0).getString(0));
            String pathJson = "/tmp/importservice1/json/" +cat+"/"+ str + ".json";
            System.out.println("Path: " + path);

            df.write().mode(SaveMode.Overwrite).json(pathJson);
            df.write().insertInto("jsonv2");
            
            
        } catch (Exception ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
    }

    private static void DruidBeam(JavaRDD<String> rdd) {

    	System.out.println("In the beam section");
    	SparkSession spark = SparkSession
                .builder()
                .appName("interfacing spark sql to hive metastore without configuration file")
                .config("hive.metastore.uris", "thrift://hh3-m0.vkcvlvqqaieujcpt4lgxtrkpia.fx.internal.cloudapp.net:9083") // replace with your hivemetastore service's thrift url
                .enableHiveSupport() // don't forget to enable hive support
                .getOrCreate();

    	Dataset<Row> dataSet = spark.read().json(rdd);
    	dataSet.foreach((ForeachFunction<Row>) row -> doBeam(row));
    			
    	


    }

    private static void doBeam(Row row)
    {
    	System.out.println("Do beam");
    	String timestamp = row.getString(row.fieldIndex("created"));
		String institution_id = row.getString(row.fieldIndex("institution_id"));
		String org_id = row.getString(row.fieldIndex("org_id"));
		String questionId = "-1";
		if (row.getString(row.fieldIndex("questionId")) != null) {
			questionId = row.getString(row.fieldIndex("questionId"));
		}
		String user_id = row.getString(row.fieldIndex("user_id"));
		String total_questions = row.getString(row.fieldIndex("total_questions"));
		String score = String.valueOf(row.getDouble(row.fieldIndex("score")));

        //GetFileName(row.get(0).getString(0));
        String str =
        row.mkString("[{",",","}]");
        System.out.println("Timestamp: " + timestamp +" data: "+str);
        SimpleEventBeam.TestBeam(timestamp,institution_id, org_id, questionId, user_id, total_questions, score);
    }
    
    private static String GetFileName(String fileName)
    {
        String result = null;

         fileName = fileName.replaceAll("-","");
         fileName = fileName.replaceAll(":","");
         result = fileName;

        return result;
    }

    private static String GetCatName(String fileName)
    {
        String result = null;

        fileName = fileName.replaceAll("-","");
        fileName = fileName.replaceAll(":","");
        fileName = fileName.replaceAll(".","");
        String[] fileNameArr = fileName.split("T");

        result = fileNameArr[0];

        return result;
    }

}



